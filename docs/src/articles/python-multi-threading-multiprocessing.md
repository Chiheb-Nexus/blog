# Python: Multithreading VS Multiprocessing

## Introduction

Les modules de threading et de multiprocessing en python visent à faire la même chose, c’est-à-dire à faire plusieurs choses en même temps, mais la façon dont le module de threading et le module de multiprocessing s’y prennent est très différente.

C’est pourquoi une définition générale s’impose:

**Process**: Est une instance d’un programme qui tourne dans un ordinateur (une machine)

Propriété des ressources: Espace d’adressage virtuel pour contenir l’image de processus, y compris le programme, les données, la pile et les attributs
L’exécution d’un processus suit un chemin à travers le programme
Commutateur de processus: Une opération coûteuse (lourde) en raison de la nécessité de sauvegarder les données de contrôle et d’enregistrer le contenu
Thread: Est une unité de répartition d’une œuvre dans un processus

**Interruptible**: le processus peut se tourner vers un autre thread
Tous les fils d’un processus partagent le code et les segments de données
Le commutateur de thread est généralement moins coûteux que le commutateur de processus
Un exemple concret: Supposons qu’on est entrain de créer une GUI (interface graphique) et dans cette dernière on aura besoin de cliquer sur un bouton pour afficher un text dans une zone précise de cette interface. De ce fait:

En utilisant le principe de multiprocessing: Le programme va créer un nouveau process, une nouvelle interface, et affichera le texte. C’est à dire, on va créer un nouveau espace d’adressage virtuel en chargeant une copie du code, les données etc.. en lançant la nouvelle fonctionnalité d’affichage du text après le clic sur le bouton. Ceci dit, le nouveau text ne sera visible que dans la nouvelle instance du programme.
En utilisant le principe de threading/Multithreading: Le programme va créer un nouveau thread au sein du process principal de l’interface graphique et lancera la fonction d’affichage du texte. Ceci dit, le thread sera lancé comme une nouvelle unité de calcul au sein du process qui traite l’interface graphique et tout le code, les données, etc.. du programme principale seront accéssible par tous les threads lancés par ce process en question.

## Différences entre Threading et Multiprocessing

### Threading

Un nouveau thread est généré dans le processus existant
Le démarrage d’un thread est plus rapide que le démarrage d’un processus
La mémoire est partagée entre tous les threads
Des mutex souvent nécessaires pour contrôler l’accès aux données partagées
Un GIL (Global Interpreter Lock) pour tous les threads (Important)
Il ne peut pas faire les choses en parallèle mais il peut faire les choses simultanément (Concurrency: Taches concurrentes), c’est-à-dire qu’il peut faire des allers-retours entre les choses quand il en a le temps. (Lire en dessous!)

### Multiprocessing

Un nouveau processus est lancé indépendamment du premier processus
Le démarrage d’un processus est plus lent que le démarrage d’un thread
La mémoire n’est pas partagée entre les processus (c’est-à-dire plus de mémoire impliquée lors du démarrage d’un processus par rapport à un thread bien que le processus enfant ait accès à la mémoire du processus parent). (Lire en dessous!)
Les mutex ne sont pas nécessaires (sauf si le threading dans le nouveau processus, c’est-à-dire que le processus enfant est en train de threader)
Un GIL (Global Interpreter Lock) pour chaque processus
A noter:

### Concurrency: Taches concurrentes

A la première vue on pourra dire que les taches concurrentes (Threading et Couroutines) ne tournent pas en parallère et ceci est vrai en absolue. Car, la définition la plus banale de ce concept dit que que les threads peuvent faire des allers-retours entre les taches quand ils auront le temps. Cependant, en diminuant la fréquence d’altération d’une tache vers une autre on pourra aboutir à un processus qui tendera vers des processus qui tournent en parallèle.

Deux processus peuvent-ils partager le même segment de mémoire partagée?

Oui et non. Généralement, avec les systèmes d’exploitation modernes, lorsqu’un autre processus est dérivé du premier, ils partagent le même espace mémoire avec un jeu de copie en écriture sur toutes les pages. Toutes les mises à jour apportées à l’une des pages de mémoire en lecture-écriture entraînent une copie de la page, il y aura donc deux copies et la page de mémoire ne sera plus partagée entre le processus parent et enfant. Cela signifie que seules les pages en lecture seule ou les pages qui n’ont pas été écrites seront partagées.

Si un processus n’a pas été issu d’un autre (aucun fork), il ne partage généralement pas de mémoire. Une exception est que si vous exécutez deux instances du même programme, elles peuvent partager du code et peut-être même des segments de données statiques, mais aucune autre page ne sera partagée.

Il existe également des appels de mappage de mémoire spécifiques pour partager le même segment de mémoire. L’appel indique si la carte est en lecture seule ou en lecture-écriture. La façon de procéder est très dépendante du système d’exploitation.

Deux threads peuvent-ils partager la même mémoire partagée?

Certainement. En général, toute la mémoire à l’intérieur d’un processus multithread est “partagée” par tous les threads. C’est généralement la définition des threads en ce sens qu’ils s’exécutent tous dans le même espace mémoire.

Les threads ont également la complexité supplémentaire d’avoir des segments de mémoire mis en cache dans la mémoire haute vitesse liés au processeur / noyau. Cette mémoire en cache n’est pas partagée et les mises à jour des pages de mémoire sont vidées dans le stockage central en fonction des opérations de synchronisation. Et c’est là où le GIL (Global Interpreter Lock) de Python intervient.

Assez de théories :D faisons un peu de code !

## Exemples

::: tip
**Threading** avec/sans Mutex
:::

```python
"""Threading with/without mutex"""

import time
import threading

MUTEX = threading.Lock()


def task(arg, result):
    # With mutex
    with MUTEX:
        result.append(arg ** 2)


def task2(arg, result):
    # GIL will prevent the simultaneous access to the list
    result.append(arg ** 2)


def run(n, switch_task=False):
    _task = task if not switch_task else task2
    threads, result, start = [], [], time.time()
    for k in range(n):
        thread = threading.Thread(
            target=_task,
            args=(k, result)
        )
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

    print(result)
    print('<{task_name}> Finished in: {duration}'.format(
        task_name=_task.__name__,
        duration=time.time() - start
    ))


if __name__ == '__main__':
    run(10)
    run(10, switch_task=True)
```

::: details Output

```bash
# Output is similar to:
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
<task> Finished in: 0.0009059906005859375
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
<task2> Finished in: 0.0006284713745117188
```

:::

:::tip
**Multiprocessing avec Mutex**: Notez dans cet exemple le fait qu’on a partagé une liste et des chiffres entre les différents process en utilisant des Mutex.
:::

```python
"""MultiProcessing using Mutex example"""

import time
from multiprocessing import Process, Lock, Array, Value


def task(index, result):
    result[index.value] = index.value ** 2


def run(n):
    mutex = Lock()
    start = time.time()
    result = Array('i', range(n), lock=mutex)
    for k in range(n):
        value = Value('i', k)
        process = Process(
            target=task,
            args=(value, result)
        )
        process.start()
        process.join()

    print(list(result))
    print("Finished in: ", time.time() - start)


if __name__ == '__main__':
    run(10)
```

::: details Output

```bash
# Output is similar to:
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
Finished in:  0.08156561851501465
```

:::

::: tip
**Multiprocessing avec une Pool**: Notez ici le fait que les processus sont indépendant les uns des autres.
:::

```python
"""Multiprocessing using Pool"""

import time
from multiprocessing import Pool, cpu_count


def task(arg):
    return arg ** 2


def run(n):
    start = time.time()
    result = Pool(cpu_count()).map(task, range(n))
    print(result)
    print("Finished in: ", time.time() - start)


if __name__ == '__main__':
    run(10)
```

::: details Output

```bash
# Output is similar to:
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
Finished in:  0.014499902725219727
```

::: tip
Coroutines en utilisant **Async/Await**
:::

```python
"""Example using coroutines"""

import time
import asyncio


async def task(num):
    # Wait 1s
    await asyncio.sleep(1)
    return num ** 2


async def run_tasks(num):
    # Wait all tasks to finish
    return await asyncio.gather(*[task(k) for k in range(num)])


def run(num):
    start = time.perf_counter()
    loop = asyncio.new_event_loop()
    try:
        result = loop.run_until_complete(run_tasks(num))
        print(result)
        print("Finished in: ", time.perf_counter() - start)
    finally:
        # We should close the loop
        loop.close()


if __name__ == '__main__':
    run(10)
```

::: details Output

```bash
# Output is similar to
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
Finished in:  1.001627395000014
```

:::

::: tip
Coroutines en utilisant **ThreadPoolExecutor**
:::

```python
"""Example using coroutines within Threads and blocking task"""

import time
import asyncio
from concurrent.futures import ThreadPoolExecutor


def blocking_task(num):
    # time.sleep is a blocking function
    time.sleep(1)
    return num ** 2


async def run_tasks(executor, num):
    loop = asyncio.get_event_loop()
    blocking_tasks = [
        loop.run_in_executor(executor, blocking_task, k)
        for k in range(num)
    ]
    # Wait for tasks to finish
    completed, pending = await asyncio.wait(blocking_tasks)
    return [elm.result() for elm in completed]


def run(num):
    start = time.perf_counter()
    # How many workers should be launched
    executor = ThreadPoolExecutor(max_workers=num)
    loop = asyncio.new_event_loop()
    try:
        result = loop.run_until_complete(run_tasks(executor, num))
        print(result)
        print("Finished in: ", time.perf_counter() - start)
    finally:
        loop.close()


if __name__ == '__main__':
    run(10)
```

::: details Output

```bash
# Output is similar to:
# Note here how the output is unordered
[1, 9, 64, 0, 16, 81, 25, 36, 4, 49]
Finished in:  1.0040447339997627
```

:::

::: tip
Coroutines en utilisant des Mutex
:::

```python
"""Example using coroutines within Mutex"""

import asyncio


MUTEX = asyncio.Lock()


async def task(num, result):
    # Wait 1s
    await asyncio.sleep(1)
    # Check if the Mutex is acquired or not in order to append to the list
    async with MUTEX:
        result.append(num ** 2)


async def run_tasks(k, result):
    _ = await asyncio.gather(*[task(k, result) for k in range(k)])


def run(num):
    result = []
    start = time.perf_counter()
    loop = asyncio.new_event_loop()
    try:
        loop.run_until_complete(run_tasks(num, result))
        print(result)
        print("Finished in: ", time.perf_counter() - start)
    finally:
        loop.close()


if __name__ == '__main__':
    run(10)
```

::: details Output

```bash
# Output is similar to:
# Note the unordered list
[4, 64, 9, 81, 0, 16, 1, 25, 36, 49]
Finished in:  1.0023566509999
```

:::

## Conclusion

**Avant de finir**: Une question qui se pose toujours: Quand dois-je utiliser le Multithreading ? Quand dois-je utiliser le Multiprocessing ? et Quand dois-je utiliser les Coroutines ?

En gros:

- **Multiprocessing**: Quand on cherche de paralléliser des processus liés à la consommation des unités de calcul (CPU bound)
- **Multithreading/Concurrency**: Quand on cherche de “paralléliser”/faire des taches concurrentes des processus liés à IO (Input Output) (IO bound)
- **Coroutines**: Quand on le peut sinon on utilise le Multithreading.

![thats All Folks](https://upload.wikimedia.org/wikipedia/commons/e/ea/Thats_all_folks.svg)
